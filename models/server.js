const express = require('express');
const cors = require('cors');

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;

    // Middlewares
    this.middlewares();
    // Inicializar rutas
    this.routes();
  }

  middlewares() {
    // Expose public directory
    this.app.use(express.static('public'));

    // Lectura y parseo del body
    this.app.use(express.json());

    // CORS
    this.app.use(cors());
  }

  routes() {
    // Users routes
    this.app.use('/api/users', require('../routes/users'));
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log(`App runnin' at port ${this.port}`);
    });
  }
}

module.exports = Server;
