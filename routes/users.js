const { Router } = require('express');

const {
  userIndex,
  userCreate,
  userUpdate,
  userDelete,
  userPatch
} = require('../controllers/users');

const router = Router();

router.get('/', userIndex);

router.post('/', userCreate);

router.put('/:id', userUpdate);

router.delete('/:id', userDelete);

router.patch('/', userPatch);

module.exports = router;
