const { request, response } = require('express');

const userIndex = (req = request, res = response) => {
  const { limit = 10 } = req.query;

  res.json({
    data: 'GET api - controller',
    limit
  });
};

const userCreate = (req = request, res = response) => {
  const { body } = req;

  res.json({
    data: body
  });
};

const userUpdate = (req = request, res = response) => {
  const { params: { id }, body } = req;

  res.json({
    data: body,
    id
  });
};

const userDelete = (req = request, res = response) => {
  const { params: { id } } = req;

  res.json({
    data: 'DELETE api - controller',
    id
  });
};

const userPatch = (req = request, res = response) => {
  res.json({
    data: 'PATCH api - controller'
  });
};

module.exports = {
  userIndex,
  userCreate,
  userUpdate,
  userDelete,
  userPatch
};
